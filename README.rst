=======
OpenREM Visualisation
=======

OpenREM Visualisation is a development fork of OpenREM http://www.openrem.org. The objective of this fork is to add reporting and visualisation features to OpenREM.

Branches
-----------

This fork contains a number of branches.

* *Develop:* this is the master development branch and includes all working local changes.
* *Tracker:* this follows the upstream repository cleanly (no local patches). Branch from here to submit clean pull requests.
* *Feature branches:* these branches contain in-progress patches for specific features.
* *Backup:* this branch contains a backup of historical files from the previous test server.

Quick start
-----------

Installation should follow the default instructions for OpenREM. Additional packages will be required and detailes of these will appear below.

Psycopg2 is required to use postgresql on Windows. A binary can be downloaded from http://www.lfd.uci.edu/~gohlke/pythonlibs/

Installing PostgreSQL
---------------------
Using Django with PostgreSQL on Windows has a few minor problems. 

On Windows Server 2012 you need to install PostgreSQL *outside* the "Program Files" directory because of permission problems. If the first install fails with "Failure to initialize cluster.." error then uninstall, right click on the folder you are installing to and change the permissions so all users can write.

Once PostgreSQL is installed django may fail to syncdb due to "Value too long for variable char field length (50)". To solve this, open pgAdmin, navigate to 

* openremdb database
* schemas
* public
* tables
* auth_permission

right click on this, select properties, columns and change "name" to varying(100) from varying(50).