
Export from database
====================

Multi-sheet Microsoft Excel XLSX exports
++++++++++++++++++++++++++++++++++++++++
This export has a summary sheet of all the requested and performed 
protocols and the series protocols. The next sheet has all studies on,
one study per line, with the series stretching off to the right. The
remaining sheets are specific to each series protocol, in alphabetical
order, with one series per line. If one study has three series with the
same protocol name, each one has a line of its own.

.. automodule:: remapp.exports.xlsx
    :members:

Single sheet CSV exports
++++++++++++++++++++++++
   
.. automodule:: remapp.exports.exportcsv
    :members:
